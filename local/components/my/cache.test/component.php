<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);
if($arParams['IBLOCK_ID'] == 0)
	return;

$arParams['ELEMENT_ID'] = intval($arParams['ELEMENT_ID']);
if($arParams['ELEMENT_ID'] == 0)
	return;
	
CModule::IncludeModule('iblock');

// Объект
$obCache = new CPHPCache();


// Время кеширования
$cacheLifetime = $arParams['CACHE_TIME']; 

// Идентификатор кеша
$cacheID = $arParams['IBLOCK_ID'] . $arParams['ELEMENT_ID'] . $USER->GetUserGroupString(); 

// Директория кеша
$cachePath = '/cache_test/';

if( $obCache->InitCache($cacheLifetime, $cacheID, $cachePath) ){
   $arVars = $obCache->GetVars();
   $arResult = $arVars['arResult'];

   $obCache->Output();
}
elseif( $obCache->StartDataCache()  ){
	
	$arSelect = Array("ID", "NAME", "PREVIEW_TEXT", "PROPERTY_COMMENT", "IBLOCK_ID");
	$arFilter = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], "ID" => $arParams['ELEMENT_ID'], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
	$rsElement = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	
	global $CACHE_MANAGER;
	$CACHE_MANAGER->StartTagCache($cachePath);
	
	if($arElement = $rsElement->GetNext())
	{
		$CACHE_MANAGER->RegisterTag("cache_test_iblock_id_" . $arElement["IBLOCK_ID"]);
		$arResult = $arElement;
	}
	
	$CACHE_MANAGER->EndTagCache();

	$this->IncludeComponentTemplate();
	
	// Сохранение переменных в кеш
	$obCache->EndDataCache(
		array(
			"arResult" => $arResult,
		)
	);
}

?>
