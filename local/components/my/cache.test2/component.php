<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);
if($arParams['IBLOCK_ID'] == 0)
	return;

$arParams['ELEMENT_ID'] = intval($arParams['ELEMENT_ID']);
if($arParams['ELEMENT_ID'] == 0)
	return;
	
CModule::IncludeModule('iblock');

if ($this->StartResultCache(false, $USER->GetGroups()))
{
	$arSelect = Array("ID", "NAME", "PREVIEW_TEXT", "DETAIL_TEXT", "PREVIEW_PICTURE", "DETAIL_PICTURE", "PROPERTY_COMMENT");
	$arFilter = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], "ID" => $arParams['ELEMENT_ID'], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
	$rsElement = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	if($arElement = $rsElement->GetNext())
	{
		$arResult = $arElement;
	}
	
	$this->SetResultCacheKeys(array(
		"ID",
		"NAME",
		"PREVIEW_TEXT",
		"PROPERTY_COMMENT_VALUE",
	));
	
	$this->IncludeComponentTemplate();
}

$APPLICATION->SetTitle($arResult['NAME']);
?>
