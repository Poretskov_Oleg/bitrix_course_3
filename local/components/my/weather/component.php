<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

// создаем объект
$obCache = new CPageCache; 

// Время кеширования
$cacheLifetime = $arParams['CACHE_TIME']; 

// Идентификатор кеша
$cacheID = 'weather_parse';

// Директория кеша
$cachePath = '/weather_cache/';

// инициализируем буферизирование вывода
if($obCache->StartDataCache($cacheLifetime, $cacheID, $cachePath)):
	
	$cityId = '28642';

	$weatherData = "http://export.yandex.ru/weather-ng/forecasts/" . $cityId . ".xml";

	$xml = simplexml_load_file($weatherData);

	$temperature = $xml->fact->temperature;
	$imageCode = $xml->fact->image;
	$weatherType = $xml->fact->weather_type;

	if ($temperature>0) $temperature = '+'.$temperature;

	?>
	<a href="http://pogoda.yandex.ru/<?=$cityId?>/">Челябинск</a> <?=$temperature?> <sup>o</sup>C <img src="http://img.yandex.net/i/wiz<?=$imageCode?>.png" alt="<?=$weatherType?>" title="<?=$weatherType?>">
	<?
    // записываем буферизированный результат на диск в файл кеша
    $obCache->EndDataCache(); 
endif;



