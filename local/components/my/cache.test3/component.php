<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);
if($arParams['IBLOCK_ID'] == 0)
	return;

CModule::IncludeModule('iblock');

if ($this->StartResultCache(false, $USER->GetGroups()))
{
	$arSelect = Array("ID", "NAME", "PREVIEW_TEXT", "DETAIL_TEXT", "PREVIEW_PICTURE", "DETAIL_PICTURE");
	$arFilter = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
	$rsElement = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($arElement = $rsElement->GetNext())
	{
		$arResult['ITEMS'][] = $arElement;
	}
	
	$this->IncludeComponentTemplate();
}

?>
