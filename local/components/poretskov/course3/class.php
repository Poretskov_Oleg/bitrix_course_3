<?

class Course3 extends CBitrixComponent
{
    public function executeComponent()
    {
        $componentPage = $this->getComponentPage();

        $this->includeComponentTemplate($componentPage);
    }

    public function getComponentPage()
    {
        $arDefaultUrlTemplates404 = array(
            "sections" => "",
            "vote" => "vote/",
            "result" => "result/",
        );

        $arVariables = array();

        $arUrlTemplates = CComponentEngine::makeComponentUrlTemplates(
            $arDefaultUrlTemplates404, $this->arParams["SEF_URL_TEMPLATES"]
        );

        $engine = new CComponentEngine($this);
        $componentPage = $engine->guessComponentPath(
            $this->arParams["SEF_FOLDER"],
            $arUrlTemplates,
            $arVariables
        );

        if(!$componentPage)
        {
            $componentPage = "sections";
        }

        $this->arResult = array(
            "FOLDER" => $this->arParams["SEF_FOLDER"],
            "URL_TEMPLATES" => $arUrlTemplates,
        );

        return $componentPage;
    }
}
