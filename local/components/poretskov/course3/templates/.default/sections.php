<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc;
?>

<a href="<?=$arParams['SEF_URL_TEMPLATES']['vote']?>"><?=Loc::getMessage('PORETSKOV_VOTE_PAGE')?></a>
<a href="<?=$arParams['SEF_URL_TEMPLATES']['result']?>"><?=Loc::getMessage('PORETSKOV_VOTE_RESULT_PAGE')?></a>
