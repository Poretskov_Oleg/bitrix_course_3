<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);
?>
    <p><a href="<?=$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["sections"]?>"><?=Loc::getMessage("PORETSKOV_BACK")?></a></p>

<?
$APPLICATION->IncludeComponent(
    "poretskov:course3.result",
    ".default",
    array(
        "IBLOCK_ID_RESPOND" => $arParams['IBLOCK_ID_RESPOND'],
        "IBLOCK_ID_RESULT" => $arParams['IBLOCK_ID_RESULT'],
        "LIST_PROPERTY_CODE" => $arParams['LIST_PROPERTY_CODE']
    ),
    $component
);
?>