<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);
?>
<form action="<?=$APPLICATION->getCurPage(false) ?>">

    <?foreach ($arResult['ITEMS'] as $item):?>

        <?if ($item['VALUES']):?>
            <div>
                <label for="<?=$item['CODE']?>"><?=$item['NAME']?></label>
                <select name="<?=$item['CODE']?>" id="<?=$item['CODE']?>">
                    <option value=""></option>
                    <?foreach ($item['VALUES'] as $keyValue => $value):?>
                        <option value="<?=$keyValue?>"><?=$value?></option>
                    <?endforeach;?>
                </select>
            </div>
        <?elseif ($item['PROPERTY_TYPE'] == 'N'):?>
            <div>
                <label for="<?=$item['CODE']?>_MIN"><?=$item['NAME']?></label>
                <input id="<?=$item['CODE']?>_MIN" name="<?=$item['CODE']?>_MIN" type="number">
                <input id="<?=$item['CODE']?>_MAX" name="<?=$item['CODE']?>_MAX" type="number">
            </div>
        <?endif;?>

    <?endforeach;?>

    <button type="submit" name="FIND">Показать</button>

</form>

<pre>
    <?print_r($arResult['FIND_ITEMS']);?>
</pre>