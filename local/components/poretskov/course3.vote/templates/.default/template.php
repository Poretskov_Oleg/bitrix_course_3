<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);
?>

<form action="<?=$APPLICATION->getCurPage(false) ?>">

    <?foreach ($arResult['ITEMS'] as $item):?>

        <div>
            <label for="<?=$item['CODE']?>"><?=$item['NAME']?></label>
            <?if ($item['VALUES']):?>

                <select name="<?=$item['CODE']?>" id="<?=$item['CODE']?>">
                    <option value=""></option>
                    <?foreach ($item['VALUES'] as $keyValue => $value):?>
                    <option value="<?=$keyValue?>"><?=$value?></option>
                    <?endforeach;?>
                </select>

            <?else:?>
                <input id="<?=$item['CODE']?>" name="<?=$item['CODE']?>" type="<?=($item['PROPERTY_TYPE'] == 'N' ? 'number' : 'text')?>">
            <?endif;?>
        </div>

    <?endforeach;?>

    <button type="submit" name="SAVE">Отправить</button>

</form>
