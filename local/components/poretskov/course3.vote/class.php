<?
use \Bitrix\Main\Loader;
use \Bitrix\Main\Application;

Loader::includeModule('iblock');

class Course3Vote extends CBitrixComponent
{
    public function executeComponent()
    {
        $this->arResult['ITEMS'] = $this->prepareItems();

        $request = Application::getInstance()->getContext()->getRequest()->toArray();

        if (isset($request['SAVE'])) {
            $this->saveVote($request);
        }

        $this->includeComponentTemplate();
    }

    public function prepareItems()
    {
        $db_enum_list = CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID" => $this->arParams["IBLOCK_ID_RESULT"]));
        $arValues = [];
        while ($ar_enum_list = $db_enum_list->GetNext())
        {
            $arValues[$ar_enum_list['PROPERTY_ID']][$ar_enum_list['ID']] = $ar_enum_list['VALUE'];
        }

        $rsProp = CIBlockProperty::GetList(
            Array("sort" => "asc"),
            Array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => $this->arParams["IBLOCK_ID_RESULT"]
            )
        );
        $arProperty = [];
        while ($arr = $rsProp->Fetch())
        {
            if (in_array($arr["CODE"], $this->arParams['FORM_PROPERTY_LIST'])) {

                if ($arr['PROPERTY_TYPE'] == 'L') {
                    $arr['VALUES'] = $arValues[$arr['ID']];
                }
                $arProperty[$arr['CODE']] = $arr;
            }
        }

        return $arProperty;
    }

    public function saveVote($request)
    {
        $el = new CIBlockElement();
        $respond = 0;
        foreach ($this->arResult['ITEMS'] as $item) {
            if ($item['PROPERTY_TYPE'] == 'E') {
                $propE = $item['CODE'];
                if (!empty($request[$propE])) {
                    $respond = $el->Add(['IBLOCK_ID' => $this->arParams['IBLOCK_ID_RESPOND'], 'NAME' => $request[$propE]]);
                }
                break;
            }
        }

        $name = $request[$propE];
        if ($respond) {
            $request[$propE] = $respond;
        } else {
            unset($request[$propE]);
        }

        $el->Add([
            'IBLOCK_ID' => $this->arParams['IBLOCK_ID_RESULT'],
            'NAME' => $name,
            'PROPERTY_VALUES' => $request
        ]);
    }
}
