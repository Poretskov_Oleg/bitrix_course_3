<?
$MESS['POR_MODULE_NAME'] = 'Модуль. Курс 3';
$MESS['POR_MODULE_DESCRIPTION'] = 'Модуль для курса 3';
$MESS['POR_PARTNER_NAME'] = 'Порецков Олег';
$MESS['POR_PARTNER_URI'] = 'https://academy.1c-bitrix.ru/education/index.php?login=yes&COURSE_ID=67&INDEX=Y';
$MESS['POR_INSTALL_TITLE'] = 'Установка модуля Модуль.Курс3';
$MESS['POR_UNINSTALL_TITLE'] = 'Удаление модуля Модуль.Курс3';
$MESS['POR_NOT_ACCESS'] = 'Нет прав для установки и удаления модуля';