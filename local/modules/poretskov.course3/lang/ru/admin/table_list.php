<?
$MESS['POR_TABLE_NAV'] = 'Элементы';
$MESS['POR_TABLE_NAME'] = 'Наименование';
$MESS['POR_EL_EDIT'] = 'Редактирование';
$MESS['POR_EL_DEL'] = 'Удаление';
$MESS['POR_DEL_CONF'] = 'Подтвердите удаление элемента';
$MESS['POR_TABLE_TITLE'] = 'Таблица Курс 3';
$MESS['POR_F_FIND'] = 'Поиск';
$MESS['POR_F_NAME'] = 'Наименование';
$MESS['POR_F_FIND_TITLE'] = 'Фильтр по нименованию или ID';