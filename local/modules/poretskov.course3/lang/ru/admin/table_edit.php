<?
$MESS["POR_TAB_NAME"] = "Элемент таблицы";
$MESS["POR_TAB_TITLE"] = "Элемент таблицы";
$MESS["POR_EL_NAME"] = "Наименование элемента";
$MESS["POR_SAVE_ERROR"] = "Ошибка сохранения данных";
$MESS["POR_TITLE_EDIT"] = "Редактирование элемента: ";
$MESS["POR_TITLE_ADD"] = "Создание элемента";
$MESS["POP_LIST"] = "Список элементов";
$MESS["POR_LIST_TITLE"] = "Возврат к списку элементов";
$MESS["POR_EL_ADD"] = "Новый элемент";
$MESS["POR_EL_ADD_TITLE"] = "Добавление нового элемента";
$MESS["POR_EL_DEL"] = "Удаление элемента";
$MESS["POR_EL_DEL_TITLE"] = "Удаление элемента";
$MESS["POR_EL_DEL_CONF"] = "Подтвердите удаление элемента";
$MESS["POR_EL_SAVED"] = "Элемент успешно сохранен";