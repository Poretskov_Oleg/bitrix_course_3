<?
namespace Poretskov\Course3\Entity;

use Bitrix\Main\Entity;
use Bitrix\Main\Type\DateTime;

class TestTable extends Entity\DataManager
{
    static public function getTableName()
    {
        return 'poretskov_course3_table';
    }

    static function getMap()
    {
        return [
            new Entity\IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true
                ]
            ),
            new Entity\StringField(
                'NAME',
                [
                    'required' => true
                ]
            ),
            new Entity\DatetimeField(
                'TIMESTAMP_X',
                [
                    'default_value' => new DateTime()
                ]
            )
        ];
    }
}