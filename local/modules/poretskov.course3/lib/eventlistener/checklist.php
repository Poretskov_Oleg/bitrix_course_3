<?
namespace Poretskov\Course3\EventListener;

use Bitrix\Main\Localization\Loc;

class CheckList
{
    public function onCheckListGet($arCheckList)
    {
        $checkList = [
            'CATEGORIES' => [],
            'POINTS' => [],
        ];

        $checkList['CATEGORIES']['MY_TEST'] = [
            'NAME' => 'Мои тесты',
            'LINKS' => ''
        ];

        $checkList['POINTS']['SUPPORT_FILE'] = [
            'PARENT' => 'MY_TEST',
            'REQUIRED' => 'N',
            'AUTO' => 'Y',
            'CLASS_NAME' => __CLASS__,
            'METHOD_NAME' => 'checkSupport',
            'NAME' => 'Проверка информации о техподдержке',
            'DESC' => 'Проверка добавления на сайт инфломации о техподдержке',
            'HOWTO' => 'Проверяется наличие файла bitrix/php_interface/this_site_support.php',
            'LINKS' => ''
        ];

        $checkList['POINTS']['TITLE'] = [
            'PARENT' => 'MY_TEST',
            'REQUIRED' => 'Y',
            'AUTO' => 'N',
            'NAME' => 'Рекомендация формирования заголовка страницы',
            'DESC' => 'Проверьте шаблон для формировния заголовка страницы',
            'HOWTO' => 'Заголовок страницы должен строиться по шаблону #Раздел 1.1#-#Раздел 1#-#Название сайта#',
            'LINKS' => ''
        ];

        return $checkList;
    }

    public function checkSupport()
    {
        $filePath = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/this_site_support.php';

        $checkFile = (file_exists($filePath) && filesize($filePath) > 0);

        if ($checkFile) {
            $arResult = [
                'STATUS' => true,
                'MESSAGE' => [
                    'PREVIEW' => 'Тест успешно пройден'
                ]
            ];
        } else {
            $arResult = [
                'STATUS' => false,
                'MESSAGE' => [
                    'PREVIEW' => 'Тест не пройден',
                    'DETAIL' => 'Файла /bitrix/php_interface/this_site_support.php нет или он пуст'
                ]
            ];
        }

        return $arResult;
    }
}