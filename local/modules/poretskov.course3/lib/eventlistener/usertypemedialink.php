<?
namespace Poretskov\Course3\EventListener;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;

Loader::includeModule("fileman");
\CMedialib::Init();

class UserTypeMediaLink extends \CUserTypeInteger
{
    public function GetUserTypeDescription()
    {
        return array(
            "USER_TYPE_ID" => 'media_link',
            "CLASS_NAME" => __CLASS__,
            "DESCRIPTION" => Loc::getMessage("USER_TYPE_MEDIA_LINK_DESCRIPTION"),
            "BASE_TYPE" => \CUserTypeManager::BASE_TYPE_INT
        );
    }

    function GetEditFormHTML($arUserField, $arHtmlControl)
    {
        $arCollection = \CMedialibCollection::GetList([
            'arOrder' => ['NAME'=>'ASC'],
            'arFilter' => ['ACTIVE' => 'Y']
        ]);

        $result = '<select name="'.$arHtmlControl["NAME"].'">';
        $result .= '<option value=""'.(!$arHtmlControl["VALUE"]? ' selected': '').'></option>';
        foreach ($arCollection as $collection) {
            $result .=
                '<option
                    value="'.$collection["ID"].'"'.
                    ($arHtmlControl["VALUE"] == $collection["ID"] ? ' selected': '').
                '>'.
                    $collection["NAME"].
                '</option>';
        }

        $result .= '</select>';

        return $result;
    }

    function GetAdminListViewHTML($arUserField, $arHtmlControl)
    {
        if(strlen($arHtmlControl["VALUE"])>0) {
            $rsEnum = \CMedialibCollection::GetList([
                'arOrder' => ['NAME' => 'ASC'],
                'arFilter' => [
                    'ACTIVE' => 'Y',
                    'ID' => $arHtmlControl["VALUE"]
                ]
            ]);
            return $rsEnum[0]['NAME'];
        } else {
            return '&nbsp;';
        }
    }
}