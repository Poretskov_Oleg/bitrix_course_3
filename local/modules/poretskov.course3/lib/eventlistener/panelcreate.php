<?
namespace Poretskov\Course3\EventListener;

use Bitrix\Main\Localization\Loc;

class panelCreate
{
    public function OnPanelCreateHandler()
    {
        global $APPLICATION;
        $APPLICATION->AddPanelButton(array(
            "TYPE" => 'BIG',
            "HREF" => "/bitrix/admin/table_list.php",
            "ICON" => "bx-panel-install-solution-icon",
            "TEXT" => Loc::getMessage('POR_PANEL_BUTTON_NAME'),
            "ALT"  => Loc::getMessage('POR_PANEL_BUTTON_TITLE'),
            "MAIN_SORT" => 700,
            "SORT" => 10
        ));
    }
}