<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

use Bitrix\Main\Localization\Loc;
use Poretskov\Course3\Entity\TestTable;
use Bitrix\Main\Loader;

IncludeModuleLangFile(__FILE__);
Loader::includeModule('poretskov.course3');

$POST_RIGHT = $APPLICATION->GetGroupRight("subscribe");
if($POST_RIGHT=="D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$aTabs = array(
    array("DIV" => "edit1", "TAB" => Loc::getMessage("POR_TAB_NAME"), "ICON" => "main_user_edit", "TITLE" => Loc::getMessage("POR_TAB_TITLE")),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$ID = intval($ID);		// Id of the edited record
$message = null;

if($REQUEST_METHOD == "POST" && ($save!="" || $apply!="") && $POST_RIGHT=="W" && check_bitrix_sessid())
{
    $arFields = Array(
        "NAME" => $NAME
    );

    if($ID > 0)
    {
        $res = TestTable::update($ID, $arFields);
    }
    else
    {
        $res = TestTable::add($arFields);
        $ID = $res->getId();
    }

    if($res->isSuccess())
    {
        if($apply!="")
            LocalRedirect("/bitrix/admin/table_edit.php?ID=".$ID."&mess=ok&lang=".LANG."&".$tabControl->ActiveTabParam());
        else
            LocalRedirect("/bitrix/admin/table_list.php?lang=".LANG);
    }
    else
    {
        if($e = $APPLICATION->GetException())
            $message = new CAdminMessage(Loc::getMessage("POR_SAVE_ERROR"), $e);
    }

}

//Edit/Add part
ClearVars();
if($ID>0)
{
    $element = TestTable::getRowById($ID);
    $NAME = $element['NAME'];
    if(!$element)
        $ID=0;
}

$APPLICATION->SetTitle(($ID>0? Loc::getMessage("POR_TITLE_EDIT").$ID : Loc::getMessage("POR_TITLE_ADD")));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$aMenu = array(
    array(
        "TEXT"=> Loc::getMessage("POP_LIST"),
        "TITLE"=> Loc::getMessage("POR_LIST_TITLE"),
        "LINK"=>"table_list.php?lang=".LANG,
        "ICON"=>"btn_list",
    )
);
if($ID>0)
{
    $aMenu[] = array("SEPARATOR"=>"Y");
    $aMenu[] = array(
        "TEXT" => Loc::getMessage("POR_EL_ADD"),
        "TITLE" => Loc::getMessage("POR_EL_ADD_TITLE"),
        "LINK"=>"table_edit.php?lang=".LANG,
        "ICON"=>"btn_new",
    );
    $aMenu[] = array(
        "TEXT" => Loc::getMessage("POR_EL_DEL"),
        "TITLE" => Loc::getMessage("POR_EL_DEL_TITLE"),
        "LINK"=>"javascript:if(confirm('".Loc::getMessage("POR_EL_DEL_CONF")."'))window.location='table_list.php?ID=".$ID."&action_button=delete&lang=".LANG."&".bitrix_sessid_get()."';",
        "ICON"=>"btn_delete",
    );
}
$context = new CAdminContextMenu($aMenu);
$context->Show();
?>

<?
if($_REQUEST["mess"] == "ok" && $ID>0)
    CAdminMessage::ShowMessage(array("MESSAGE" => Loc::getMessage("POR_EL_SAVED"), "TYPE"=>"OK"));

if($message)
    echo $message->Show();
elseif($res && !$res->isSuccess())
    CAdminMessage::ShowMessage($res->getErrorMessages());
?>

<form method="POST" Action="<?echo $APPLICATION->GetCurPage()?>" ENCTYPE="multipart/form-data" name="post_form">
<?
$tabControl->Begin();
$tabControl->BeginNextTab();
?>
    <tr class="adm-detail-required-field">
        <td><?echo Loc::getMessage("POR_EL_NAME")?></td>
        <td><input type="text" name="NAME" value="<?echo $NAME;?>" size="45" maxlength="100"></td>
    </tr>
<?
$tabControl->Buttons(
    array(
        "disabled"=>($POST_RIGHT<"W"),
        "back_url"=>"table_list.php?lang=".LANG,

    )
);
?>
<?echo bitrix_sessid_post();?>
    <input type="hidden" name="lang" value="<?=LANG?>">
<?if($ID>0 && !$bCopy):?>
    <input type="hidden" name="ID" value="<?=$ID?>">
<?endif;?>
<?
$tabControl->End();
?>

<?
$tabControl->ShowWarnings("post_form", $message);
?>

    <script language="JavaScript">
        <!--
        if(document.post_form.AUTO.checked)
            tabControl.EnableTab('edit2');
        else
            tabControl.DisableTab('edit2');
        //-->
    </script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>