<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Poretskov\Course3\Entity\TestTable;
use Bitrix\Main\Application;

Loc::loadLanguageFile(__FILE__);
Loader::includeModule('poretskov.course3');

global $APPLICATION;

$permissionRight = $APPLICATION->GetGroupRight('poretskov.course3');
if ($permissionRight == 'D')
{
    $APPLICATION->AuthForm(Loc::getMessage('ACCESS_DENIED'));
}
// подготовка данных

$sTableID = 'course3_table';
$oSort = new CAdminSorting($sTableID, 'ID', 'DESC');
$lAdmin = new CAdminList($sTableID, $oSort);

function CheckFilter()
{
    global $lAdmin, $FilterArr;
    foreach ($FilterArr as $f) {
        global $$f;
    }
    return count($lAdmin->arFilterErrors) == 0;
}

$FilterArr = [
    'find',
    'find_type',
    'find_id',
    'find_name'
];

$lAdmin->InitFilter($FilterArr);
$arFilter = [];
if (CheckFilter()) {
    $id = ($find != '' && $find_type == 'id' ? $find : $find_id);
    if($id) {
        $arFilter['ID'] = $id;
    }
    $name = ($find!="" && $find_type == "name"? $find : $find_name);
    if($name) {
        $arFilter['NAME'] = $name;
    }
}

// обработка действий над элементами списка
// сохранение отредактированных элементов
if ($permissionRight == 'W' && $lAdmin->EditAction()) {
    foreach ($FIELDS as $ID => $arFields) {
        if(!$lAdmin->IsUpdated($ID)) {
            continue;
        }

        $DB->StartTransaction();

        if ($row = TestTable::getRowById($ID)) {

            foreach ($arFields as $key => $value) {
                $row[$key] = $value;
            }

            $result = TestTable::update($ID, $row);

            if(!$result->isSuccess()) {
                $DB->Rollback();
                $lAdmin->AddGroupError($result->getErrors());
            }

        } else {
            $DB->Rollback();
            $lAdmin->AddGroupError('Ошибка обновления данных');
        }

        $DB->Commit();
    }
}
$request = Application::getInstance()->getContext()->getRequest();
// обработка одиночных и групповых
if(($arID = $lAdmin->GroupAction()) && $permissionRight=="W")
{
    if($request['action_target'] == 'selected')
    {
        $rsData = TestTable::getList(['filter' => $arFilter]);
        while($arRes = $rsData->fetch())
            $arID[] = $arRes['ID'];
    }

    foreach($arID as $ID)
    {
        if(strlen($ID)<=0)
            continue;
        $ID = IntVal($ID);

        switch($request['action_button'])
        {
            case "delete":
                $DB->StartTransaction();

                $result = TestTable::delete($ID);
                if(!$result->isSuccess())
                {
                    $DB->Rollback();
                    $lAdmin->AddGroupError($result->getErrorMessages(), $ID);
                }
                $DB->Commit();
                break;
        }
    }
}
$sort = [];
if($request['by'] && $request['order']) {
    $sort = [strtoupper($request['by']) => $request['order']];
}

$rsData = TestTable::getList(['filter' => $arFilter, 'order' => $sort]);
$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();
$lAdmin->NavText($rsData->GetNavPrint(Loc::getMessage("POR_TABLE_NAV")));

$lAdmin->AddHeaders(array(
    array(
        "id"		=> "ID",
        "content"	=> "ID",
        "sort"		=> "id",
        "align"		=> "right",
        "default"	=> true,
    ),
    array(
        "id"		=> "NAME",
        "content"	=> Loc::getMessage("POR_TABLE_NAME"),
        "sort"		=> "name",
        "default"	=> true,
    ),
    array(
        "id"		=> "TIMESTAMP_X",
        "content"	=> "TIMESTAMP_X",
        "sort"		=> "timestamp_x",
        "default"	=> true,
    ),
));

while($arRes = $rsData->NavNext(true, "f_")):
    $row =& $lAdmin->AddRow($f_ID, $arRes);

    $row->AddInputField("NAME", array("size"=>20));
    $row->AddViewField("NAME", '<a href="table_edit.php?ID='.$f_ID.'&amp;lang='.LANG.'">'.$f_NAME.'</a>');

    $arActions = Array();

    $arActions[] = array(
        "ICON" => "edit",
        "DEFAULT" => true,
        "TEXT" => Loc::getMessage("POR_EL_EDIT"),
        "ACTION" => $lAdmin->ActionRedirect("table_edit.php?ID=".$f_ID)
    );
    if ($permissionRight >= "W") {
        $arActions[] = array("SEPARATOR" => true);
        $arActions[] = array(
            "ICON" => "delete",
            "TEXT" => Loc::getMessage("POR_EL_DEL"),
            "ACTION" => "if(confirm('" . Loc::getMessage('POR_DEL_CONF') . "')) " . $lAdmin->ActionDoGroup($f_ID, "delete")
        );
    }

    $row->AddActions($arActions);
endwhile;

$lAdmin->AddFooter(
    array(
        array("title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$rsData->SelectedRowsCount()),
        array("counter" => true, "title" => GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"),
    )
);
$lAdmin->AddGroupActionTable(Array(
    "delete"=>GetMessage("MAIN_ADMIN_LIST_DELETE"),
));

$aContext = array(
    array(
        "TEXT" => Loc::getMessage("MAIN_ADD"),
        "LINK" => "table_edit.php?lang=".LANG,
        "TITLE" => Loc::getMessage("POST_ADD_TITLE"),
        "ICON" => "btn_new",
    ),
);
$lAdmin->AddAdminContextMenu($aContext);
$lAdmin->CheckListMode();

$APPLICATION->SetTitle(Loc::getMessage("POR_TABLE_TITLE"));

require_once($_SERVER['DOCUMENT_ROOT']. '/bitrix/modules/main/include/prolog_admin_after.php');
// вывод страницы

$oFilter = new CAdminFilter(
    $sTableID."_filter",
    array(
        "ID",
        Loc::getMessage("POR_F_NAME"),
    )
);
?>
<!--Вывод фильтра-->
<form name="find_form" method="get" action="<?echo $APPLICATION->GetCurPage();?>">
<?$oFilter->Begin();?>
<tr>
	<td><b><?=Loc::getMessage("POR_F_FIND")?>:</b></td>
	<td>
		<input type="text" size="25" name="find" value="<?echo htmlspecialcharsbx($find)?>" title="<?=Loc::getMessage("POR_F_FIND_TITLE")?>">
		<?
		$arr = array(
			"reference" => array(
				"ID",
				Loc::getMessage("POR_F_NAME"),
			),
			"reference_id" => array(
				"id",
				"name",
			)
		);
		echo SelectBoxFromArray("find_type", $arr, $find_type, "", "");
		?>
	</td>
</tr>
<tr>
	<td><?="ID"?>:</td>
	<td>
		<input type="text" name="find_id" size="47" value="<?echo htmlspecialcharsbx($find_id)?>">
	</td>
</tr>
<tr>
	<td><?=Loc::getMessage("POR_F_NAME")?>:</td>
	<td>
		<input type="text" name="find_name" size="47" value="<?echo htmlspecialcharsbx($find_name)?>">
	</td>
</tr>
<?
$oFilter->Buttons(array("table_id"=>$sTableID,"url"=>$APPLICATION->GetCurPage(),"form"=>"find_form"));
$oFilter->End();
?>
</form>
<!--Вывод фильтра конец-->

<?$lAdmin->DisplayList();?>

<?
require_once($_SERVER['DOCUMENT_ROOT']. '/bitrix/modules/main/include/epilog_admin.php');