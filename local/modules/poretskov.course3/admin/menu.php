<?
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if ($APPLICATION->GetGroupRight("poretskov.course3") <= 'D')
{
	return false;
}

$aMenu = array(
	"parent_menu" => "global_menu_services",
	"section" => "course3",
	"sort" => 700,
	"text" => Loc::getMessage("POR_TABLE_TEXT"),
	"title" => Loc::getMessage("POR_TABLE_TITLE"),
	"icon" => "iblock_menu_icon_iblocks",
	"page_icon" => "iblock_page_icon_elements",
	"items_id" => "menu_por_table",
	"items" => array()
);

$aMenu['items'][] = array(
	"text" => Loc::getMessage("POR_TABLE_LIST"),
	"url" => "table_list.php?lang=".LANGUAGE_ID,
	"more_url" => array("table_list.php"),
	"title" => Loc::getMessage("POR_TABLE_LIST")
);

$aMenu['items'][] = array(
	"text" => Loc::getMessage("POR_TABLE_EDIT"),
	"url" => "table_edit.php?lang=".LANGUAGE_ID,
	"more_url" => array("table_edit.php"),
	"title" => Loc::getMessage("POR_TABLE_EDIT")
);

return $aMenu;