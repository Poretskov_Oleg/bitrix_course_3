<?

use Bitrix\Main\Localization\Loc;
use \Bitrix\Main\ModuleManager;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Entity\Base;
use \Poretskov\Course3\Entity\TestTable;
use Bitrix\Main\Application;
use \Bitrix\Main\EventManager;
use Bitrix\Main\IO\Directory;

Loc::loadMessages(__FILE__);

class poretskov_course3 extends \CModule
{
    public $MODULE_ID = 'poretskov.course3';
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $PARTNER_NAME;
    public $PARTNER_URI;

    /**
     * poretskov_course3 constructor.
     */
    public function __construct()
    {
        $arModuleVersion = [];
        include 'version.php';

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = Loc::getMessage('POR_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('POR_MODULE_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('POR_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('POR_PARTNER_URI');
    }

    /**
     * @return bool|void
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\SystemException
     */
    public function InstallDB()
    {
        Loader::includeModule($this->MODULE_ID);

        if (
            !Base::getInstance('\Poretskov\Course3\Entity\TestTable')
                ->getConnection()->isTableExists(TestTable::getTableName())
        ) {
            Base::getInstance('\Poretskov\Course3\Entity\TestTable')->createDbTable();
        }
    }

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\Db\SqlQueryException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\SystemException
     */
    public function UnInstallDB()
    {
        Loader::includeModule($this->MODULE_ID);

        if (Base::getInstance('\Poretskov\Course3\Entity\TestTable')->getConnection()->isTableExists(TestTable::getTableName())) {
            $connection = Application::getConnection();
            $connection->queryExecute('drop table '. TestTable::getTableName());
        }
    }

    /**
     * @return bool|void
     */
    public function InstallFiles()
    {
        \CopyDirFiles(__DIR__.'/admin', $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin', true, true);
        \CopyDirFiles(__DIR__.'/components', $_SERVER['DOCUMENT_ROOT'].'/local/components', true, true);

        return true;
    }

    /**
     * @return bool|void
     */
    public function UnInstallFiles()
    {
        \DeleteDirFiles(__DIR__.'/admin', $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin');
        Directory::deleteDirectory($_SERVER['DOCUMENT_ROOT'].'/local/components/poretskov');
        return true;
    }

    public function InstallEvents()
    {
        EventManager::getInstance()->registerEventHandler(
            'main',
            'OnUserTypeBuildList',
            $this->MODULE_ID,
            'Poretskov\\Course3\\EventListener\\UserTypeMediaLink',
            'GetUserTypeDescription'
        );

        EventManager::getInstance()->registerEventHandler(
            'main',
            'OnPanelCreate',
            $this->MODULE_ID,
            'Poretskov\\Course3\\EventListener\\panelCreate',
            'OnPanelCreateHandler'
        );

        EventManager::getInstance()->registerEventHandler(
            'main',
            'onCheckListGet',
            $this->MODULE_ID,
            'Poretskov\\Course3\\EventListener\\CheckList',
            'onCheckListGet'
        );
    }

    public function UnInstallEvents()
    {
        EventManager::getInstance()->unRegisterEventHandler(
            'main',
            'OnUserTypeBuildList',
            $this->MODULE_ID,
            'Poretskov\\Course3\\EventListener\\UserTypeMediaLink',
            'GetUserTypeDescription'
        );

        EventManager::getInstance()->unRegisterEventHandler(
            'main',
            'OnPanelCreate',
            $this->MODULE_ID,
            'Poretskov\\Course3\\EventListener\\panelCreate',
            'OnPanelCreateHandler'
        );

        EventManager::getInstance()->unRegisterEventHandler(
            'main',
            'onCheckListGet',
            $this->MODULE_ID,
            'Poretskov\\Course3\\EventListener\\CheckList',
            'onCheckListGet'
        );
    }

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\SystemException
     */
    public function DoInstall()
    {
        global $APPLICATION;

        if ($APPLICATION->GetUserRight("main") < 'W') {
            $APPLICATION->ThrowException(Loc::getMessage('POR_NOT_ACCESS'));
            return false;
        }

        ModuleManager::registerModule($this->MODULE_ID);

        $this->InstallDB();
        $this->InstallFiles();
        $this->InstallEvents();
        $APPLICATION->IncludeAdminFile(Loc::getMessage('POR_INSTALL_TITLE'), __DIR__.'/step.php');
        return true;
    }

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\Db\SqlQueryException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\SystemException
     */
    public function DoUninstall()
    {
        global $APPLICATION, $step;

        if ($APPLICATION->GetUserRight("main") < 'W') {
            $APPLICATION->ThrowException(Loc::getMessage('POR_NOT_ACCESS'));
            return false;
        }

        $step = IntVal($step);

        if ($step < 2) {
            $APPLICATION->IncludeAdminFile(Loc::getMessage('POR_UNINSTALL_TITLE'), __DIR__.'/unstep1.php');
        } elseif ($step == 2) {

            $request = Application::getInstance()->getContext()->getRequest();
            if ($request['save_tables'] !== 'Y') {
                $this->UnInstallDB();
            }

            ModuleManager::unRegisterModule($this->MODULE_ID);
            $this->UnInstallFiles();
            $this->UnInstallEvents();
            $APPLICATION->IncludeAdminFile(Loc::getMessage('POR_UNINSTALL_TITLE'), __DIR__.'/unstep.php');
        }

        return true;
    }
}