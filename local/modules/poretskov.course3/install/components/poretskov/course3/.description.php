<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;

$arComponentDescription = array(
	"NAME" => Loc::getMessage("COMPONENT_PORETSKOV_COURSE3_NAME"),
	"DESCRIPTION" => Loc::getMessage("COMPONENT_PORETSKOV_COURSE3_DESCRIPTION"),
	"COMPLEX" => "Y",
	"PATH" => array(
		"ID" => "poretskov",
		"NAME" => Loc::getMessage('SECTION_PORETSKOV_NAME')
	),
);

?>