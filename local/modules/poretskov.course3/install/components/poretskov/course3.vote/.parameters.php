<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

use \Bitrix\Main\Localization\Loc;

$arIBlock=array();
$rsIBlock = CIBlock::GetList(Array("SORT" => "ASC"), Array("TYPE" => 'course3', "ACTIVE"=>"Y"));
while($arr=$rsIBlock->Fetch())
{
	$arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
}

$arProperty_LNS = array();
$rsProp = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arCurrentValues["IBLOCK_ID_RESULT"]));
while ($arr=$rsProp->Fetch())
{
	$arProperty[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
	if (in_array($arr["PROPERTY_TYPE"], array("L", "N", "S", "E")))
	{
		$arProperty_LNS[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
	}
}

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"IBLOCK_ID_RESPOND" => array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage("IBLOCK_ID_RESPOND"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlock,
		),
		"IBLOCK_ID_RESULT" => array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage("IBLOCK_ID_RESULT"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlock,
			"REFRESH" => "Y",
		),
		"FORM_PROPERTY_LIST" => array(
			"PARENT" => "BASE",
			"NAME" => Loc::getMessage("FORM_PROPERTY_LIST"),
			"TYPE" => "LIST",
			"MULTIPLE" => "Y",
			"VALUES" => $arProperty_LNS,
		),
		"CACHE_TIME"  =>  Array("DEFAULT"=>36000000),
	),
);
?>
