<?
use \Bitrix\Main\Loader;
use \Bitrix\Main\Application;
use \Bitrix\Iblock\ElementTable;
use \Bitrix\Iblock\ElementPropertyTable;

Loader::includeModule('iblock');

class Course3Vote extends CBitrixComponent
{
    public function executeComponent()
    {
        $this->arResult['ITEMS'] = $this->prepareItems();

        $this->arResult['FIND_ITEMS'] = $this->findItems();

        $this->includeComponentTemplate();
    }

    public function findItems()
    {
        $arFilter['IBLOCK_ID'] = $this->arParams["IBLOCK_ID_RESULT"];
        $arSelect = ['ID', 'IBLOCK_ID', 'NAME'];
        foreach ($this->arParams['LIST_PROPERTY_CODE'] as $code) {
            $arSelect[] = 'PROPERTY_'.$code;
        }

        $request = Application::getInstance()->getContext()->getRequest();

        if (isset($request['FIND'])) {

            foreach ($this->arParams['LIST_PROPERTY_CODE'] as $code) {

                if (
                    $this->arResult['ITEMS'][$code]['PROPERTY_TYPE'] == 'N' &&
                    (!empty($request[$code.'_MIN']) || !empty($request[$code.'_MAX']))
                ) {
                    $min = $request[$code.'_MIN']?:0;
                    $max = $request[$code.'_MAX']?:0;
                    $arFilter['><PROPERTY_'.$code] = [$min, $max];
                } elseif (!empty($request[$code])) {
                    $arFilter['PROPERTY_'.$code] = $request[$code];
                }
            }
        }

        $dbRes = CIBlockElement::GetList(
            [], $arFilter, false, false, $arSelect
        );
        $res = [];
        while($element = $dbRes->fetch()) {
            $res[] = $element;
        }

        return $res;
    }

    public function prepareItems()
    {
        $db_enum_list = CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID" => $this->arParams["IBLOCK_ID_RESULT"]));
        $arValues = [];
        while ($ar_enum_list = $db_enum_list->GetNext())
        {
            $arValues[$ar_enum_list['PROPERTY_ID']][$ar_enum_list['ID']] = $ar_enum_list['VALUE'];
        }

        $rsProp = CIBlockProperty::GetList(
            Array("sort" => "asc"),
            Array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => $this->arParams["IBLOCK_ID_RESULT"]
            )
        );
        $arProperty = [];
        while ($arr = $rsProp->Fetch())
        {
            if (in_array($arr["CODE"], $this->arParams['LIST_PROPERTY_CODE'])) {

                if ($arr['PROPERTY_TYPE'] == 'L') {
                    $arr['VALUES'] = $arValues[$arr['ID']];
                }
                $arProperty[$arr['CODE']] = $arr;
            }
        }

        return $arProperty;
    }
}
