<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div id="news-list" class="news-list">
    <? if ($arParams['IS_AJAX_PAGINATION']) {
        $APPLICATION->RestartBuffer();
    } ?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<p class="news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <img
            class="preview_picture"
            border="0"
            src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
            width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
            height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
            alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
            title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
            data-bx-viewer="image"
            data-bx-title="<?=$arItem["NAME"]?>"
            data-bx-src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>"
            data-bx-download="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>"
            data-bx-width="548"
            data-bx-height="346"
            style="float:left"
            />

        <b class="name" data-id="<?=$arItem["ID"]?>"><?=$arItem["NAME"]?></b>
        <div id="<?=$arItem["ID"]?>" style="display:none;">
            <b><?echo $arItem["DETAIL_TEXT"]?></b>
        </div>
        <br />
	</p>
<?endforeach;?>
	<br />
    <div id="pagination">
    <?=$arResult["NAV_STRING"]?>
    </div>
</div>
<script>
    BX.ready(function () {
        function easyShow(node) {
            var easingAppear = new BX.easing({
                duration: 900,
                start: {height: 0, opacity: 0},
                finish: {height: 100, opacity: 100},
                transition: BX.easing.transitions.quart,
                step: function (state) {
                    node.style.height = state.height + "px";
                    node.style.opacity = state.opacity / 100;
                },
            });
            easingAppear.animate();
        }
        function easyHide(node) {
            var easingDelete = new BX.easing({
                duration: 900,
                start: {height: 100, opacity: 100},
                finish: {height: 0, opacity: 0},
                transition: BX.easing.transitions.quart,
                step: function (state) {
                    node.style.height = state.height + "px";
                    node.style.opacity = state.opacity / 100;
                },
            });
            easingDelete.animate();
        }
        // используем ajax для пагинации без перезагрузки страницы
        function DEMOLoad(url) {
            // используем easing для анимации
            easyHide(BX("news-list"));
            BX.ajax.post(
                url,
                {'is_ajax' : 'Y'},
                DEMOResponse
            );
        }
        // обработка ajax ответа
        function DEMOResponse(html) {
            BX("news-list").innerHTML = html;
            // используем easing для анимации
            easyShow(BX("news-list"));
        }
        // вешаем ajax на клик по кнопкам пагинации
        BX.bindDelegate(
            BX('pagination'), 'click', {tagName: 'a'},
            function (e) {
                if (!e) {
                    e = window.event;
                }
                // получить ссылку на кнопке пагинации
                var url = BX(e.target).getAttribute('href');
                // ссылка на первую страницу не содержит параметр PAGEN - добавляем его
                if (url.indexOf('?PAGEN_1') === -1) {
                    url = url + '?PAGEN_1=1';
                }
                DEMOLoad(url);
                return BX.PreventDefault(e);
            }
        );
    });
</script>

<? if ($arParams['IS_AJAX_PAGINATION']) {
    die();
} ?>
<script>
    BX.ready(function() {

        var obImageView = BX.viewElementBind(
            'news-list',
            {showTitle: true, lockScroll: false},
            function(node){
                return BX.type.isElementNode(node) && (node.getAttribute('data-bx-viewer') || node.getAttribute('data-bx-image'));
            }
        );

        var oPopup = new BX.PopupWindow('detail_text', window.body, {
            autoHide: true,
            offsetTop: 1,
            offsetLeft: 0,
            lightShadow: true,
            closeIcon: true,
            closeByEsc: true,
        });
        BX.bindDelegate(
            BX('news-list'), 'click', {className: 'name'},
            function (e) {
                console.log(this.getAttribute('data-id'));
                oPopup.setContent(BX(this.getAttribute('data-id')).innerHTML);
                oPopup.show();
                return BX.PreventDefault(e);
            }
        );
    });
</script>